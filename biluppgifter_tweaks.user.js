// ==UserScript==
// @name         Biluppgifter.se - Tweaks
// @namespace    http://tampermonkey.net/
// @version      2.5
// @description  Visa ägare, ta bort onödigt trams, mm
// @author       You
// @match        https://biluppgifter.se/fordon/*
// @grant        GM.xmlHttpRequest
// @connect      merinfo.se
// ==/UserScript==


let owner_card = document.querySelector(".card-owner");
let merinfo_link = owner_card.querySelector(".gtm-merinfo");

// Owner info
make_owner_card(merinfo_link.href);

// Show all vehicles from the same owner
var all_link = document.querySelector('a[href*="/brukare/"]');
if (all_link) {
    make_vehicles_card(all_link.href);
}

// Remove crap
remove_element(document.querySelector("section.grade"));
remove_element(document.querySelector("#box-summary div.col-auto"));

let crap_cards = ["box-lead", "box-insurance", "box-ads", "box-debt", "box-valuation", 
                  "box-loan", "box-video-ad", "biluppgiftere_pano1_6", 
                  "biluppgiftere_pano2_9", "au-su-container"];
for (let c of crap_cards) {
    let el = document.getElementById(c);
    if (el) {
        remove_element(el);
    }
}


function remove_element(el) {
    el.parentNode.removeChild(el);
}


function xp(exp, doc) {
    return doc.evaluate(exp, doc, null, XPathResult.ANY_TYPE, null).iterateNext();
}


function make_owner_card(merinfo_url) {
    // Get SSN
    var ssn = "-";
    GM.xmlHttpRequest({
        method: "GET",
        url: merinfo_url,
        onload: function (response) {
            let parser = new DOMParser();
            let doc = parser.parseFromString(response.responseText, "text/html");
            let address_text = xp("//strong[contains(text(), 'Folkbokföringsadress')]/../following-sibling::p", doc).innerHTML;
            let ssn_long = xp("//span[contains(@class, 'text-nowrap')]/a[contains(@href, '/user/login?return=http')]/..", doc).innerHTML;
            ssn = ssn_long.substring(1, 9);

            let owner_info = document.createElement("div");
            owner_info.innerHTML = (
                `<p>${address_text}<br>
                 ${ssn}</p>`);

            let col = owner_card.querySelectorAll('.col')[1];
            col.prepend(owner_info);
        }
    });
}

// Other vehicles with the same owner
async function make_vehicles_card(all_vehicles_url) {
    let response = await fetch(all_vehicles_url);
    let parser = new DOMParser();
    let html = await response.text();
    let doc = parser.parseFromString(html, "text/html");

    let title = doc.querySelector("h1.card-title");
    if (title) {
        title.textContent = "Alla fordon från samma ägare";
    }
    let all_card = doc.querySelector("main.container div.row.mb-4");
    let owner_box = document.querySelector("#box-owner");
    owner_box.insertAdjacentElement("afterend", all_card.cloneNode(true));
}