// ==UserScript==
// @name         Strava Show wall clock time in infobox
// @version      0.1
// @description  Show wall clock time in the infobox on the elevation profile
// @author       You
// @match        https://www.strava.com/activities/*
// @grant        none
// @run-at       document-end
// ==/UserScript==

(function () {
    let dateString = document.getElementsByTagName('time')[0].textContent;
    let timere = /([\d]{1,2}):([\d]{1,2}) [AP]M/;

    if (!timere.test(dateString)) {
        // No time available on this activity
        return;
    }

    let reMatch = dateString.match(timere);

    let startHours = parseInt(reMatch[1]);
    let startMins = parseInt(reMatch[2]);

    if (reMatch[0].includes("PM")) {
        startHours += 12;
    }

    let clockArray = ['🕛','🕐','🕑','🕒','🕓','🕔','🕕','🕖','🕗','🕘','🕙','🕚'];

    function callback(mutations, observer) {
        for (let m of mutations) {
            if (m.type === 'attributes') {
                if (m.target.textContent == null || m.target.textContent.length > 10) continue;
                if (!(/:/.test(m.target.textContent))) continue;

                let hours = 0;
                let mins = 0;
                let secs = 0;
                if ((m.target.textContent.match(/:/g)).length == 2) {
                    let ttmatch = m.target.textContent.match(/([\d]{1,2}):([\d]{1,2}):([\d]{1,2})/);
                    hours = parseInt(ttmatch[1]);
                    mins = parseInt(ttmatch[2]);
                    secs = parseInt(ttmatch[3]);
                }
                else {
                    let ttmatch = m.target.textContent.match(/([\d]{1,2}):([\d]{1,2})/);
                    mins = parseInt(ttmatch[1]);
                    secs = parseInt(ttmatch[2]);
                }

                let clockMins = startMins + mins;
                let clockHours = startHours + hours + (parseInt(clockMins / 60));
                clockMins %= 60;


                let infoText = `${m.target.textContent} (${clockArray[(parseInt(clockHours) % 12 )]} ${clockHours}:${clockMins.toString().padStart(2, "0")})`;

                m.target.textContent = infoText;
            }
        }
    }

    setTimeout(function() {
        let cross = document.getElementById('crossBar');
        let toolTip = cross.getElementsByTagName('text')[0];

        const observer = new MutationObserver(callback);
        observer.observe(toolTip, {characterData: true, attributes: true});

    }, 1000);

})();