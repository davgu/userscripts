// ==UserScript==
// @name         SMHI Väderradar
// @version      0.9
// @description  Större karta, autouppdaterande. Minikarta som favicon
// @match        https://www.smhi.se/vader/radar-och-satellit/radar-blixt/*
// @grant        none
// ==/UserScript==

document.querySelector('#extContent').remove()
document.querySelector('._2iwYz').remove();
document.querySelector('.global-top-header').remove()

let map_widget = document.querySelector('._1A4ob');
map_widget.style = `position: fixed !important;
	top: 0 !important;
	left: 0 !important;
	right: 0 !important;
	bottom: 0 !important;
	width: 100% !important;
	height: 100% !important;
	margin: 0 !important;
	min-width: 0 !important;
	max-width: none !important;
	min-height: 0 !important;
	max-height: none !important;
	box-sizing: border-box !important;
	object-fit: contain;
	transform: none !important;`;

document.getElementsByTagName('body')[0].style.overflow = 'hidden';

// Center map in viewport
window.dispatchEvent(new Event('resize'));

// Center on current location
try {
    document.querySelector('[aria-label="Hitta nuvarande position"]').click();
}
catch (e) {}

// Put slider at the bottom
document.querySelector('._23Y0O').style = `width: 90%;
	position: fixed;
	top: calc(100vh - 150px);
	left: 50px;
	margin-right: 50px;`;

// Control slider with arrowkeys
let slider = document.querySelector('._3uddS');
function checkKey(e) {
    switch (e.key) {
        case "ArrowLeft":
        case "ArrowRight":
            slider.focus();
            break;
    }
}
document.onkeydown = checkKey;

// Show time
document.querySelector('._15llh').style = `
	background: RGBA(255, 255, 255, 0.4);
	z-index: 1;
	color: black;`;

// Mini weather radar as favicon
function updateFavicon() {
    let now = new Date();
    let date = now.toISOString().slice(0, 10);
    let min = (now.getUTCMinutes() - 5 + 60) % 60; // Use time 5 minutes ago to make sure image exists on server
    let closest_five = 10*Math.floor(min / 10);
    closest_five += min - closest_five > 4 ? 5 : 0;
    let hours = now.getUTCHours() - (now.getUTCMinutes() < 5 ? 1 : 0); // Roll back an hour if five minutes ago was last hour
    let time = "T" + hours.toString().padStart(2,0) + ":" + closest_five.toString().padStart(2,0) + ":00Z";
    let URI_time = encodeURIComponent(date + time);

    let old_favicon = document.querySelector("link[rel*='icon']");
    let favicon = document.createElement('link');
    favicon.type = 'image/png';
    favicon.rel = 'shortcut icon';

    // BBOX is the bounding box (lower left x, lower left y, upper right x, upper right y)
    // https://epsg.io for coordinates in EPSG format
    // Lower left (Tranås)
    const lower_left = "1661129.48,7984306.30";
    // Upper right (Skogsby)
    const upper_right = "1815838.09,8129842.35";
    favicon.href = `https://wts2.smhi.se/tile/?=&service=WMS&request=GetMap&layers=\
baltrad%3Aradarcomp-lightning_sweden_wpt&styles=&format=image%2Fpng&transparent=true&version=1.1.1&\
time=${URI_time}&width=16&height=16&srs=EPSG%3A900913&bbox=${lower_left},${upper_right}`;

    document.head.removeChild(old_favicon);
    document.head.appendChild(favicon);
}
updateFavicon();

// Auto update
let btn = document.querySelector('[title="Hämta senaste datan"]');
setInterval(() => { btn.click(); updateFavicon(); }, 5*60*1000);
