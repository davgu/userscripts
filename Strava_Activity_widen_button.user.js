// ==UserScript==
// @name         Strava Activity widen button
// @namespace    http://tampermonkey.net/
// @version      0.3
// @description  Adds a button to fill screen horizontally
// @match        https://www.strava.com/activities/*
// @grant        none
// ==/UserScript==

let social = document.querySelector('#heading > header > .social');
let widen_button = document.createElement('div');
widen_button.innerHTML = "⟷";
widen_button.id = 'widen-button';

let widen_style = `
#widen-button {
	width: 30px;
	height: 95%;
	font-size: 18pt;
	text-align: center;
	display: flex;
	padding: 0 10px;
	align-items: center;
	justify-content: center;
	cursor: pointer;
}
#widen-button:hover {
	background-color: #fff;
`;

var style = document.createElement("style");
style.type = "text/css";
style.appendChild(document.createTextNode(widen_style));
document.getElementsByTagName("head")[0].appendChild(style);;

widen_button.addEventListener("click",
                              function() {
    let page = document.getElementsByClassName('page')[0];
    page.style.transition = 'width 0.5s ease-in-out';
    page.style.width = '100%';

    let graph = document.getElementById('chart-container').getElementsByTagName('svg')[0];
    graph.style = "margin-left: auto; margin-right: auto; display: block;";

    let splits = document.getElementById('splits-container');
    if (splits) {
        const split_width = 240;
        splits.style.width = `${split_width}px`;

        let map_container = splits.nextElementSibling;
        map_container.style.width = `calc(100% - ${split_width}px)`;

        document.getElementById('contents').style.scrollbarWidth = 'thin';
    }
});

social.insertAdjacentElement('afterbegin', widen_button);
