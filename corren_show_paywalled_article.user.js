// ==UserScript==
// @name        Corren.se - Visa betalartikel
// @namespace   none
// @include     https://login.ntm.eu/*
// @include     https://pren.corren.se/*
// @include     https://pren.nt.se/*
// @version     7.7
// @grant       GM_xmlhttpRequest
// @connect     corren.se
// @connect     nt.se
// @run-at document-start
// ==/UserScript==

var article_id = window.location.toString().match(/((?:\D{2})?\d+)\.aspx/)[1];
var domain = window.location.host == 'pren.nt.se' ? 'nt.se' : 'corren.se';

document.head.innerHTML += `<link rel="Stylesheet" href="https://www.${domain}/public/less/application.less">`;
addScript(`https://www.${domain}/public/js/lib/jquery.js`);
addScript(`https://www.${domain}/public/js/app.m.js`);

GM_xmlhttpRequest({
    method: "GET",
    url: `http://www.${domain}/ws/GetArticle.ashx?articleid=${article_id}`,
    onload: response => {
        document.body.innerHTML = `<div id="wrapper" class="wrapper-artview">
                                   <div class="content-wrapper"><div class="article-inf">
                                   ${JSON.parse(response.responseText).d.html}</div></div></div>`;
        document.querySelector('.swiper-loading').style.display = 'none';
        
        let embed = document.querySelector('.live-center-embed');
        if (embed) {
            embed.innerHTML += `<a href=${embed.dataset.src}>Klicka för live-uppdatering</a>`;
        }
        
        let heading = document.querySelector('h1.article-header');
        if (heading) {
            setTimeout(() => { document.title = heading.textContent; }, 500);
            let sp = new URLSearchParams(window.location.href.split("?").pop());
            let actual_url = sp.get("redirect");
            if (actual_url) {
                let link = document.createElement("a");
                link.href = actual_url;
                link.style = "color: #000000; text-decoration: none;"
                heading.insertAdjacentElement("beforebegin", link);
                link.appendChild(heading);
            }
        }
    }
});

function addScript(url) {
    let js_el = document.createElement('script');
    js_el.type = "text/javascript";
    js_el.src = url;
    document.head.appendChild(js_el);
}
