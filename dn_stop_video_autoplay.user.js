﻿// ==UserScript==
// @name         DN.se - Stop video autoplay
// @namespace    none
// @version      0.3
// @description  Remove autoplay from videos
// @match        *://*.dn.se/*
// @grant        none
// @run-at       document-body
// ==/UserScript==

let videos = document.querySelectorAll('dn-resource[data-type=video]');
for (const v of videos) {
    v.dataset.autoplay = false;
    
    v.addEventListener('playing', autopause, true);
}

function autopause() {
    this.querySelector('video').pause();
    this.removeEventListener('playing', autopause, true);
}
