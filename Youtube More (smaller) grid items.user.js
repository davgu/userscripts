// ==UserScript==
// @name         Youtube More (smaller) grid items
// @namespace    http://tampermonkey.net/
// @version      1
// @description  Change the CSS variable that defines how wide and how many grid items are
// @author       You
// @match        https://www.youtube.com/
// @grant        none
// ==/UserScript==

// The original CSS variable is actually defined on the parent node,
// but I can't find a way to access that since it's a custom element.
document.getElementById('contents').style.setProperty('--ytd-rich-grid-items-per-row', 6)